/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dzenkinzsample;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Student
 */
public class DzenkinzSampleTest {
    
    public DzenkinzSampleTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of randomLTThousand method, of class DzenkinzSample.
     */
    @Test
    public void testRandomLTThousandRange() {
        System.out.println("randomLTThousand");
        int result = DzenkinzSample.randomLTThousand();
        assertTrue(result >= 0);
        assertTrue(result < 1000);
    }

}
